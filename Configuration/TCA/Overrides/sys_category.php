<?php

defined('TYPO3_MODE') or die();

$sModel = basename(__FILE__, '.php');

$GLOBALS['TCA'][$sModel]['ctrl']['label'] = 'title';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt'] = 'parent';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt_force'] =  1;