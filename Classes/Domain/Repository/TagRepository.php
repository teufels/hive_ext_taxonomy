<?php
namespace HIVE\HiveExtTaxonomy\Domain\Repository;

/***
 *
 * This file is part of the "hive_ext_taxonomy" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * The repository for Tags
 */
class TagRepository extends \HIVE\HiveCfgRepository\Domain\Repository\AbstractRepository
{
    public function initializeObject()
    {
        $sUserFuncModel = 'HIVE\\HiveExtTaxonomy\\Domain\\Model\\Tag';
        $sUserFuncPlugin = 'tx_hiveexttaxonomy';
        parent::overrideQuerySettings($sUserFuncModel, $sUserFuncPlugin);
    }
}
